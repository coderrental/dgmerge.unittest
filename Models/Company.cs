﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CspMergeUnitTest.Models
{
    public class CompanyContact
    {
        public int ContactType { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string Department { get; set; }
        public string UserId { get; set; }
        public string Notes { get; set; }
        public object Company { get; set; }
        public int Id { get; set; }
    }

    public class CompanyConsumer
    {
        public string Base_Domain { get; set; }
        public string Base_Publication_Parameters { get; set; }
        public bool Active { get; set; }
        public int Default_Language_Id { get; set; }
        public int Fallback_Language_Id { get; set; }
        public int Companies_Id { get; set; }
        public int Companies_Consumers_Id { get; set; }
    }

    public class Company : IComparable
    {
        public int ProjectId { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public object County { get; set; }
        public string WebsiteUrl { get; set; }
        public bool IsConsumer { get; set; }
        public bool IsSupplier { get; set; }
        public string GlobalBinLocalPath { get; set; }
        public string GlobalBinBaseUrl { get; set; }
        public string ConsumerBinLocalPath { get; set; }
        public string ConsumerBinBaseUrl { get; set; }
        public string SupplierBinLocalPath { get; set; }
        public string SupplierBinBaseUrl { get; set; }
        public string DisplayName { get; set; }
        public object ParentId { get; set; }
        public string Type { get; set; }
        public object ExternalCode { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public int LegacyId { get; set; }
        public int LegacyParentId { get; set; }
        public CompanyContact CompanyContact { get; set; }
        public CompanyConsumer CompanyConsumer { get; set; }
        public int Companies_Id { get; set; }
        public List<CompanyParameter> CompanyParameter { get; set; }

        public Company()
        {
            CompanyParameter = new List<CompanyParameter>();
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            int notEqual = 0;
            JObject jCompany = obj as JObject;
            if (jCompany != null)
            {
                string coInfo = this.CompanyName;// + this.CompanyContact.FirstName + this.CompanyContact.LastName + this.CompanyContact.EmailAddress;
                string jCoInfo = jCompany["CompanyName"].ToString();// + jCompany["CompanyContact"]["FirstName"].ToString() + jCompany["CompanyContact"]["LastName"].ToString() + jCompany["CompanyContact"]["EmailAddress"].ToString();
                int jCoLangId = 0;
                int.TryParse(jCompany["CompanyConsumer"]["DefaultLanguageId"].ToString(), out jCoLangId);
                if (string.Compare(coInfo, jCoInfo, StringComparison.InvariantCultureIgnoreCase) != 0 || 
                    this.CompanyConsumer.Default_Language_Id != jCoLangId ||
                    string.Compare(this.CompanyConsumer.Base_Domain, jCompany["CompanyConsumer"]["BaseDomain"].ToString()) != 0)
                {
                    notEqual = 1;
                }
            }
            else
            {
                return 1;
            }
            return notEqual;
        }
    }
}
