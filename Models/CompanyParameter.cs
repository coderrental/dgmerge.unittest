﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CspMergeUnitTest.Models
{
    public class CompanyParameter
    {
        public int Companies_Id { get; set; }
        public int Companies_Parameter_Types_Id { get; set; }
        public string Value_Text { get; set; }
        public int Companies_Parameters_Id { get; set; }
        public string ParameterName { get; set; }
    }
}
