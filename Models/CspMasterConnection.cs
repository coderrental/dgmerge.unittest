﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CspMergeUnitTest.Models
{
    public class CspMasterConnection
    {
        public int ID { get; set; }
        public string Mode { get; set; }
        public string Project { get; set; }
        public string Connection { get; set; }
        public string Domain { get; set; }
    }
}
