﻿using CspMergeUnitTest.Helpers;
using CspMergeUnitTest.Models;
using Dapper;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CspMergeUnitTest
{
    class Program
    {
        static readonly ILog _log = LogManager.GetLogger(typeof(Program));
        private static string _cspMasterConnStr = "";
        private static string _cspProjectConnStr = "";
        private static string _dataFileFolder = "";
        private static JArray _projectList = new JArray(), _companyList = new JArray(), _companyParameters = new JArray(), _companyParamTypes = new JArray();
        private static List<CspMasterConnection> _listProjectConnection = new List<CspMasterConnection>();
        static void Main(string[] args)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            
            _log.Debug("App started");
            LoadConfiguration();
            LoadDataFiles();
            if (_projectList.Count == 0)
            {
                Console.WriteLine("End with error.");
                Console.ReadKey();
                return;
            }
            InitMasterConn();
            StartCompare();
            // the code that you want to measure comes here
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine($"Finish testing in {elapsedMs}ms");
            Console.ReadKey();
        }

        private static void InitMasterConn()
        {
            cTripleDes des = cTripleDes.CspTDes();
            using (SqlConnection masterConn = new SqlConnection(_cspMasterConnStr))
            {
                try
                {
                    masterConn.Open();
                }
                catch (Exception exc)
                {
                    _log.Error("Failed to connect to CSP Master");
                }
                _listProjectConnection = masterConn.Query<CspMasterConnection>("SELECT * FROM Connections").ToList();
                for (var i = 0; i < _listProjectConnection.Count; i++)
                {
                    _listProjectConnection[i].Connection = des.Decrypt(_listProjectConnection[i].Connection);
                }
            }
        }

        private static async void StartCompare()
        {
            foreach(JObject prj in _projectList)
            {
                var prjConnInfo = _listProjectConnection.FirstOrDefault(x => x.Project.ToLower() == prj["Name"].ToString().ToLower() && x.Mode.ToLower() == prj["Mode"].ToString().ToLower());
                if (prjConnInfo == null)
                {
                    continue;
                }
                using (SqlConnection prjDbCon = new SqlConnection(prjConnInfo.Connection))
                {
                    try
                    {
                        prjDbCon.Open();
                    }
                    catch(Exception exc)
                    {
                        _log.Error($"Failed to connect to{prjConnInfo.Project} with connstr {prjConnInfo.Connection}");
                        continue;
                    }
                    var jCompanies = _companyList.Where(x => x["ProjectId"].ToString() == prj["Id"].ToString()).ToList();
                    var jCoIds = jCompanies.Select(x => int.Parse(x["Id"].ToString())).ToList();
                    var jCoParams = _companyParameters.Where(x => jCoIds.IndexOf(int.Parse(x["CompanyId"].ToString())) >= 0).ToList();
                    await CompareCompany(prj, prjDbCon, jCompanies);
                    await CompareCompanyParameter(prj, prjDbCon, jCoParams);
                }
                _log.Info($"Finished compare for {prjConnInfo.Project}");  
            }
            
        }

        private static async Task CompareCompanyParameter(JObject prj, SqlConnection prjDbCon, List<JToken> jCoParams)
        {
            var sqlQuery = @"select co.*, cp.companies_parameters_Id, cpt.companies_parameter_types_Id, cp.value_text, cpt.parametername from companies co
join companies_parameters cp on co.companies_Id = cp.companies_Id
join companies_parameter_types cpt on cp.companies_parameter_types_Id = cpt.companies_parameter_types_Id";
            var companyDic = new Dictionary<int, Company>();
            var coParamList = prjDbCon.Query<Company, CompanyParameter, Company>(sqlQuery,
                (company, coParam) =>
                {
                    Company co;
                    if (!companyDic.TryGetValue(company.Companies_Id, out co))
                    {
                        co = company;
                        co.CompanyParameter = new List<CompanyParameter>();
                        companyDic.Add(company.Companies_Id, co);
                    }
                    co.CompanyParameter.Add(coParam);
                    return co;
                }, splitOn: "companies_parameters_Id").Distinct().ToList();
            //foreach (var co in coParamList)
            Parallel.ForEach(coParamList, (co) =>
            {
                var jParamList = jCoParams.Where(x => int.Parse(x["LegacyCompanyId"].ToString()) == co.Companies_Id).ToList();
                if (jParamList.Count != co.CompanyParameter.Count)
                {
                    _log.Info($"{prj["Name"].ToString()} - COUNT Parameter of legacy id {co.Companies_Id} not match between export({jParamList.Count}) and DB({co.CompanyParameter.Count})");
                }
                foreach (var param in co.CompanyParameter)
                {
                    var jParam = jParamList.FirstOrDefault(x => int.Parse(x["LegacyCompanyParameterTypeId"].ToString()) == param.Companies_Parameter_Types_Id);
                    if (jParam == null) continue;
                    if (!param.Value_Text.Equals(jParam["Value"].ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        _log.Info($"{prj["Name"].ToString()} - Parameter [{param.ParameterName}] of legacy id {co.Companies_Id} not match \"{param.Value_Text}\" vs \"{jParam["Value"].ToString()}\"");
                    }
                }
            });
        }

        private static async Task CompareCompany(JObject prj, SqlConnection prjDbCon, List<JToken> jCompanies)
        {
            var prjCompanies = prjDbCon.Query<Company, CompanyConsumer, Company>(@"select * from companies c
                join companies_consumers cc on c.companies_Id = cc.companies_Id",
                (company, companyconsumer) =>
                {
                    company.CompanyConsumer = companyconsumer;
                    return company;
                },
                splitOn: "companies_Id,companies_Id").Distinct().ToList();
            //var jCompanies = _companyList.Where(x => x["ProjectId"].ToString() == prj["Id"].ToString()).ToList();
            if (jCompanies.Count != prjCompanies.Count)
            {
                _log.Info($"*** {prj["Name"].ToString()} - Company not match between export({jCompanies.Count}) and DB({prjCompanies.Count})");
            }

            Parallel.ForEach(_companyList, (coJObject) =>
            //foreach(var coJObject in jCompanies)
            {
                var legacyId = 0;
                int.TryParse(coJObject["LegacyId"].ToString(), out legacyId);
                var dbCo = prjCompanies.FirstOrDefault(x => x.Companies_Id == legacyId);
                if (dbCo == null)
                {
                    _log.Info($"{prj["Name"]} - Company {legacyId} not found in project DB data");
                    return;
                }
                if (dbCo.CompareTo(coJObject) != 0)
                {
                    _log.Info($"{prj["Name"]} - Company {legacyId} not matched to company in DB data");
                }
            });
        }

        private static void LoadDataFiles()
        {
            if (!Directory.Exists(_dataFileFolder))
            {
                _log.Error("Data folder not found");
                return;
            }
            var prjFile = ConfigurationManager.AppSettings["ProjectFile"];
            var coFile = ConfigurationManager.AppSettings["CompanyFile"];
            var coParamFile = ConfigurationManager.AppSettings["CompanyParameterFile"];
            var paramTypeFile = ConfigurationManager.AppSettings["CompanyParameterTypeFile"];
            if (!File.Exists($"{_dataFileFolder}\\{prjFile}") ||
                !File.Exists($"{_dataFileFolder}\\{coFile}") ||
                !File.Exists($"{_dataFileFolder}\\{coParamFile}") ||
                !File.Exists($"{_dataFileFolder}\\{paramTypeFile}"))
            {
                _log.Error("Missing data file");
                return;
            }
            _projectList = LoadCspData(prjFile);
            _companyList = LoadCspData(coFile);
            _companyParameters = LoadCspData(coParamFile);
            _companyParamTypes = LoadCspData(paramTypeFile);
        }

        private static JArray LoadCspData(string fileName)
        {
            var cspData = new JArray();
            try
            {
                using (StreamReader file = File.OpenText($"{_dataFileFolder}\\{fileName}"))
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    cspData = (JArray)JToken.ReadFrom(reader);
                }
            }
            catch (Exception exc)
            {
                _log.Error($"LoadCspData failed {fileName}: {exc.StackTrace}");
            }
            return cspData;
        }

        private static void LoadConfiguration()
        {
            _cspMasterConnStr = ConfigurationManager.ConnectionStrings["CspMasterConnection"].ConnectionString;
            _dataFileFolder = ConfigurationManager.AppSettings["DataFolder"];
            
        }
    }
}
